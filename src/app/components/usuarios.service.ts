import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  usuarioURL = 'http://localhost:8000/api/users';

  /**
   * Cabeceras que permiten al cliente y al servidor
   * enviar información junto la petición o respuesta.
   */
   httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(private http: HttpClient) { }

  getUsuarios(): Observable<any> {
    return this.http.get(this.usuarioURL);
  }
}
