import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaUsuariosComponent } from './tabla-usuarios/tabla-usuarios.component';
import { UsuariosService } from './usuarios.service';



@NgModule({
  declarations: [TablaUsuariosComponent],
  imports: [
    CommonModule,


  ],
  providers:[UsuariosService],
  exports:[TablaUsuariosComponent]
})
export class UsuariosModule { }
