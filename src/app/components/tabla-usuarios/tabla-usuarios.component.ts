import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-tabla-usuarios',
  templateUrl: './tabla-usuarios.component.html',
  styleUrls: ['./tabla-usuarios.component.scss']
})
export class TablaUsuariosComponent implements OnInit {

  cabeceras = ["Nombre", "Fecha Nacimiento", "Edad", "Dirección"];
  datos:any = [];
  constructor(private usuarioService:UsuariosService) { }

  ngOnInit(): void {

    this.usuarioService.getUsuarios().subscribe( data => {
      if(data.length != 0){
        this.datos =data;
      }

    }
      );
  }

  getDomicilios(domicilios:any){
    let array_dom: any[] = [];
    domicilios.map((element:any) => {
      array_dom.push(element.domicilio +' #'+ element.numero_exterior+', '+element.colonia+', CP. '+element.cp+'. '+element.ciudad)
    });
    return array_dom;
  }

}
